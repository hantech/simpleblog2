// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Simple Blog',
  siteUrl: 'https://hantech.gitlab.io',
  pathPrefix: '/simpleblog',
  outputDir: 'dist',
  plugins: [
    {
      use: 'gridsome-source-storyblok',
      options: {
        client: {
          accessToken: 'zU9Js4IoSvAGRaogK8Lx3Att' // you must be replace with your token
        }
      }
    }
  ]
}